require 'net/http'
require 'uri'
require 'json'

module Solar
  module Simulator
    class Network 

      def self.send_payload(data, options = {})
        payload = { p: {
          se: options[:serial],
          ts: data[:timestamp],
          ty: "gr",
          cfg: options[:type].to_i || 0,
          d: data[:values]
        }
        }
        uri = URI.parse(options[:uri])
        req = Net::HTTP::Post.new(uri.request_uri)
        req["content-type"] = "application/json"
        req.body = payload.to_json
        Net::HTTP.start(uri.host, uri.port) { |http|http.request(req) }

      end
     
    end
  end
end


