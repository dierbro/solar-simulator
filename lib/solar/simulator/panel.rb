require 'active_support/time'
module Solar
  module Simulator
    class Panel
      MonthToSeason = [3, 3, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3]
      Seasons = [[3240, 700],[3600, 600],[3120, 850],[2880, 1000]]
      GammaVoltage = 11
      GammaCurrent = 12
      Types = [:single_phase, :three_phase]

      def self.run(options = {})
        options[:time] ||= Time.now
        options[:nominal_power] ||= 6000.0
        options[:max_current] = options[:nominal_power]/230.0
        options[:type] = Types[options[:type].to_i || 0]
        timestamp = options[:time].to_i
        
        phases = options[:type] == :single_phase ? 1 : 3

        lux = self.simulate_lux(options)
        voltage = phases.times.map{self.simulate_voltage(options)}
        current = phases.times.map{self.simulate_current(lux, options)}

        payload = {}
        payload[:timestamp] = timestamp
        payload[:values] = voltage.zip(current).flatten << lux
        return payload
      end
     
      def self.simulate_lux(options)
        slot = (options[:time]-options[:time].beginning_of_day).to_i/15
        season = Seasons[MonthToSeason[options[:time].month-1]]
        lux = ( 1/(season[1] * Math.sqrt(2*Math::PI) ) ) * Math.exp(-((slot-season[0])**2.0)/(2*(season[1]**2.0)))
        return (lux*1539344).ceil
      end
      
      def self.simulate_voltage(options)
        sign = Random.rand(2)
        delta = rand*GammaVoltage
        base_voltage = 230

        if sign
          (base_voltage + delta).round(2)
        else
          (base_voltage - delta).round(2)
        end
      end
      
      def self.simulate_current(lux, options)
        max_delta = (options[:max_current] * lux / 1023.0) * (GammaCurrent / 100.0)
        delta = rand * max_delta
        if Random.rand(2)
          ((options[:max_current]*lux/1023.0)+delta).round(2)
        else
          ((options[:max_current]*lux/1023.0)-delta).round(2)
        end
      end
    end
  end
end

