require 'spec_helper'

describe Solar::Simulator::Panel do
  subject {Solar::Simulator::Panel}
  it "should respond to ::run" do
    should respond_to(:run).with(1).arguments
  end
  it "should respond to ::simulate_lux" do
    should respond_to(:simulate_lux).with(1).arguments
  end
  it "should respond to ::simulate_voltage" do
    should respond_to(:simulate_voltage).with(1).arguments
  end
  it "should respond to ::simulate_current" do
    should respond_to(:simulate_current).with(2).arguments
  end

  context "::run" do
    it "should return an hash" do
      puts subject.run
      expect(subject.run).to be_kind_of(Hash)
    end
    context "returned Hash" do
      let(:payload){subject.run}
      it "should have a timestamp " do
        payload[:timestamp].should_not be_nil
      end
      it "should have values " do
        payload[:values].should_not be_nil
      end
    end
  end
end

