# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'solar/simulator/version'

Gem::Specification.new do |spec|
  spec.name          = "solar-simulator"
  spec.version       = Solar::Simulator::VERSION
  spec.authors       = ["Diego R. Brogna"]
  spec.email         = ["dierbro@gmail.com"]
  spec.description   = %q{Realistic solar panel simulator}
  spec.summary       = %q{Realistic solar panel simulator}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "activesupport", "~> 4.0.0"
  spec.add_dependency "daemons"
  spec.add_dependency "slop"
  
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
end
